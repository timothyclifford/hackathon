﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Services;

namespace Hackathon.Website.Controllers
{
    public class StackOverflowController : Controller
    {
        [WebMethod]
        public JsonResult GetStackQuestions(string tag)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    var response = webClient.DownloadString("https://api.stackexchange.com/docs/search#order=desc&sort=activity&filter=default&site=stackoverflow&tagged=" + tag);

                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception exception)
            {
                return Json(exception.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
