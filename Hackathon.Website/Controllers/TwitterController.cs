﻿using System.Web.Mvc;
using System.Web.Services;
using Hackathon.Models.Helper;
using TweetSharp;

namespace Hackathon.Website.Controllers
{
    public class TwitterController : Controller
    {
        public ActionResult Authorize()
        {
            var twitterService = new TwitterService(TwitterAuthentication.Key, TwitterAuthentication.Secret);

            var requestToken = twitterService.GetRequestToken();

            Session["RequestToken"] = requestToken;

            var uri = twitterService.GetAuthorizationUri(requestToken);

            return new RedirectResult(uri.ToString());
        }

        [WebMethod]
        public JsonResult AuthorizeCallback(string verificationCode)
        {
            var requestToken = Session["RequestToken"] as OAuthRequestToken;

            if (requestToken == null)
                return Json(false, JsonRequestBehavior.AllowGet);

            var service = new TwitterService(TwitterAuthentication.Key, TwitterAuthentication.Secret);

            var accessToken = service.GetAccessToken(requestToken, verificationCode);
            
            Session["AccessToken"] = accessToken;

            service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult IsAuthorized()
        {
            var authorized = GetAuthenticatedService() != null;

            return Json(authorized, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult GetTweets(int page)
        {
            var service = GetAuthenticatedService();

            if (service == null)
                return Json(false, JsonRequestBehavior.AllowGet);

            var tweets = service.ListTweetsOnHomeTimeline(page, 10);

            return Json(tweets, JsonRequestBehavior.AllowGet);
        }

        private TwitterService GetAuthenticatedService()
        {
            try
            {
                var service = new TwitterService(TwitterAuthentication.Key, TwitterAuthentication.Secret);

                var accessToken = Session["AccessToken"] as OAuthAccessToken;

                if (accessToken == null)
                    return null;

                service.AuthenticateWith(accessToken.Token, accessToken.TokenSecret);

                var twitterUser = service.VerifyCredentials();

                return twitterUser == null ? null : service;
            }
            catch
            {
                return null;
            }
        }
    }
}
