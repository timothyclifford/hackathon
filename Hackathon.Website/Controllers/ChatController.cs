﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Services;
using Hackathon.Models.View;

namespace Hackathon.Website.Controllers
{
    public class ChatController : Controller
    {
        private const string Key = "ChatUsers";

        public ActionResult Index()
        {
            var viewModel = new ChatViewModel();
            return View(viewModel);
        }

        [WebMethod]
        public JsonResult Login(string name)
        {
            name = name.Trim().ToLower();

            var userList = HttpContext.Cache[Key] as List<string> ?? new List<string>();

            if (userList.Contains(name))
                return Json(false, JsonRequestBehavior.AllowGet);

            userList.Add(name);

            HttpContext.Cache[Key] = userList;

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult UserExists(string name)
        {
            name = name.Trim().ToLower();

            var userList = HttpContext.Cache[Key] as List<string> ?? new List<string>();

            var exists = userList.Count > 0 && userList.Contains(name);

            return Json(exists, JsonRequestBehavior.AllowGet);
        }

        [WebMethod]
        public JsonResult GetAllUsers()
        {
            var userList = HttpContext.Cache[Key] as List<string> ?? new List<string>();

            return Json(userList, JsonRequestBehavior.AllowGet);
        }
    }
}