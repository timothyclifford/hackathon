﻿var Hackathon = {};

Hackathon.Dom = {
    loadingModal: "#LoadingModal",
    // Chat
    chatContainer: ".chat-col",
    buttonChatLogin: "#ChatLoginButton",
    inputName: "#ChatLoginInput",
    buttonChatUser: ".user-list li",
    inputMessage: "#Message",
    buttonMessage: "#Send",
    // Twitter
    loadingLogin: "#LoadingLogin",
    loadingMore: "#LoadingMore",
    twitterContainer: ".twitter-col",
    inputAuthorize: "#AuthorizeInput",
    buttonAuthorize: "#AuthorizeButton",
    buttonLoadMore: "#LoadMoreTweets",
    // StackOverflow
    inputSearch: "#SearchInput",
    buttonSearch: "#SeachGo"
};

Hackathon.Helper = {
    cookieName: "ChatUsername",
    createCookie: function (value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        document.cookie = Hackathon.Helper.cookieName + "=" + value + expires + "; path=/";
    },
    readCookie: function () {
        var nameEq = Hackathon.Helper.cookieName + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(nameEq) == 0) {
                return c.substring(nameEq.length, c.length);
            }
        }
        return null;
    },
    eraseCookie: function () {
        Hackathon.Helper.createCookie("", -1);
    },
    ignorantEqual: function (s1, s2) {
        return s1.toLowerCase() === s2.trim().toLowerCase();
    }
};

Hackathon.Models = {

    Message: function (to, from, message) {
        var self = this;
        self.to = ko.observable(to),
        self.from = ko.observable(from),
        self.message = ko.observable(message),
        self.renderMessage = ko.computed(function () {
            var date = new Date(),
                dateFormat = date.getHours() + ":" + (date.getMinutes() < 10 ? ("0" + date.getMinutes()) : date.getMinutes());
            return "<strong>@" + dateFormat + " | " + self.from() + ":</strong> " + self.message();
        });
    },

    User: function (name) {
        var self = this;
        self.name = ko.observable(name);
    },

    Tweet: function (name, alias, profileUrl, imageUrl, text, when) {
        var self = this;
        self.name = ko.observable(name),
        self.alias = ko.observable(alias),
        self.profileUrl = ko.observable(profileUrl),
        self.imageUrl = ko.observable(imageUrl),
        self.text = ko.observable(text);
        self.when = ko.observable(when);
        self.renderLink = ko.computed(function () {
            return "<a href='" + self.profileUrl() + "' target='_blank'>" + self.name() + "</a><span>@" + self.alias() + "</span>";
        });
        self.renderImage = ko.computed(function () {
            return "<a href='" + self.profileUrl() + "' target='_blank'><img src='" + self.imageUrl() + "' alt='Profile image' /></a>";
        });
        self.renderText = ko.computed(function () {
            var theText = self.text();
            if (theText.indexOf("RT ") === 0) {
                var original = theText.substring(3, theText.indexOf(": ")),
                    linkUrl = original.substring(original.indexOf("http://"), original.indexOf("target") - 2),
                    linkTxt = original.substring(original.indexOf("@"), original.indexOf("</a>"));
                return theText.substring(theText.indexOf(": ") + 1) + "<br/><span class='retweet' title='Retweet'><a href='" + linkUrl + "' target='_blank'>" + linkTxt + "</a></span>";
            }
            else {
                return theText;
            }
        });
    },

    ViewModel: function () {
        var self = this;
        self.twitterLoading = ko.observable(true);
        self.chatEnabled = ko.observable(false);
        self.twitterEnabled = ko.observable(false);
        self.users = ko.observableArray([]);
        self.messages = ko.observableArray([]);
        self.tweets = ko.observableArray([]);
        self.questions = ko.observableArray([]);
        self.addMessage = function (message) {
            self.messages.push(message);
        };
        self.addUser = function (user) {
            self.users.push(user);
        };
        self.addTweet = function (tweet) {
            self.tweets.push(tweet);
        };
        self.addQuestion = function (question) {
            self.questions.push(question);
        };
    }
};

Hackathon.Server = (function () {

    var connection = {},

    initSignalR = function () {
        connection = $.connection('/chatservice');
        connection.received(update);
        connection.start();
    },

    send = function (data) {
        connection.send(data);
    },

    update = function (data) {
        var m = JSON.parse(data);
        switch (m.type) {
            case "chat":
                $.each(m.to, function (idx, el) {
                    if (Hackathon.Helper.ignorantEqual(el, Hackathon.Chat.getUser())) {
                        var message = new Hackathon.Models.Message(el, m.from, m.message);
                        Hackathon.View.addMessage(message);
                    }
                });
                break;
            case "user":
                var user = new Hackathon.Models.User(m.name);
                Hackathon.View.addUser(user);
                break;
            default:
                break;
        }
    };

    return {

        init: function () {
            initSignalR();
        },

        send: function (data) {
            send(data);
        }
    };
})();

Hackathon.View = (function () {

    var viewModel = {},

    initKnockout = function () {
        viewModel = new Hackathon.Models.ViewModel();
        ko.applyBindings(viewModel);
    };

    return {

        init: function () {
            initKnockout();
        },

        loading: {
            twitterComplete: function () {
                viewModel.twitterLoading(false);
            }
        },

        toggleChat: function (toggle) {
            viewModel.chatEnabled(toggle);
        },

        toggleTwitter: function (toggle) {
            viewModel.twitterEnabled(toggle);
        },

        addMessage: function (message) {
            viewModel.addMessage(message);
        },

        addUser: function (user) {
            viewModel.addUser(user);
        },

        addTweet: function (tweet) {
            viewModel.addTweet(tweet);
        },

        addQuestion: function (question) {
            viewModel.addQuestion(question);
        }
    };
})();

Hackathon.Chat = (function () {

    var user = "",

    recipients = [],

    bind = function () {
        $(document)
            .on("click", Hackathon.Dom.buttonChatLogin, login)
            .on("click", Hackathon.Dom.buttonMessage, sendMessage)
            .on("click", Hackathon.Dom.buttonChatUser, function () {
                $(this).toggleClass("btn-inverse");
                recipients = [];
                $(".user-list .btn-inverse").each(function (idx, el) {
                    recipients.push($(el).html());
                });
            });
    },

    checkCookie = function () {
        var cookie = Hackathon.Helper.readCookie();
        if (cookie) {
            checkUserExists(cookie);
        }
        else {
            Hackathon.View.toggleChat(false);
        }
    },

    checkUserExists = function (name) {
        toggleError(false, "");
        $.ajax({
            url: "/Chat/UserExists?name=" + name,
            type: "GET",
            success: function (data) {
                if (data) {
                    user = name;
                    Hackathon.View.toggleChat(true);
                    getUsers();
                }
                else {
                    user = "";
                    Hackathon.View.toggleChat(false);
                    Hackathon.Helper.eraseCookie();
                }
            }
        });

    },

    login = function () {
        toggleError(false, "");
        var name = $(Hackathon.Dom.inputName).val();
        if (name === "") {
            toggleError(true, "Please enter your name");
            return;
        }
        $.ajax({
            url: "/Chat/Login?name=" + name,
            type: "GET",
            success: function (data) {
                if (data) {
                    user = name;
                    getUsers();
                    Hackathon.View.toggleChat(true);
                    Hackathon.Helper.createCookie(name, 7);
                    var signalUpdate = {
                        name: name,
                        type: "user"
                    };
                    Hackathon.Server.send(JSON.stringify(signalUpdate));
                }
                else {
                    toggleError(true, "Sorry that name is taken!");
                }
            }
        });
    },

    toggleError = function (show, error) {
        var errorContainer = $(Hackathon.Dom.chatContainer).find(".control-group"),
            errorElement = errorContainer.find(".help-block");
        if (show) {
            errorContainer.addClass("error");
            errorElement.html(error);
        }
        else {
            errorContainer.removeClass("error");
            errorElement.html("");
        }
    },

    getUsers = function () {
        $.ajax({
            url: "/Chat/GetAllUsers",
            type: "GET",
            cache: false,
            success: function (data) {
                $.each(data, function (idx, el) {
                    if (!Hackathon.Helper.ignorantEqual(el, user)) {
                        var u = new Hackathon.Models.User(el);
                        Hackathon.View.addUser(u);
                    }
                });
            },
            error: function (error) {
            }
        });
    },

    sendMessage = function () {
        if ($(Hackathon.Dom.inputMessage).val() === "") {
            toggleError(true, "Please enter a message");
            return;
        }
        if (recipients.length === 0) {
            toggleError(true, "Please select recipient/s");
            return;
        }
        toggleError(false, "");
        var message = {
            from: user,
            to: recipients,
            message: $(Hackathon.Dom.inputMessage).val(),
            type: "chat"
        };
        Hackathon.Server.send(JSON.stringify(message));
        $(Hackathon.Dom.inputMessage).val("");
    };

    return {

        init: function () {
            checkCookie();
            bind();
        },

        getUser: function () {
            return user;
        }
    };
})();

Hackathon.Twitter = (function () {

    var currentPage = 1,

    bind = function () {
        $(document)
            .on("click", Hackathon.Dom.buttonAuthorize, function (e) {
                e.preventDefault();
                toggleError(false);
                $(Hackathon.Dom.loadingLogin).fadeIn(300, function () {
                    var code = $(Hackathon.Dom.inputAuthorize).val();
                    authorize(code);
                });
            }).on("click", Hackathon.Dom.buttonLoadMore, function (e) {
                e.preventDefault();
                $(Hackathon.Dom.loadingMore).fadeIn(300, function () {
                    getTweets(++currentPage);
                });
            });

    },

    authorize = function (code) {
        toggleError(false, "");
        $.ajax({
            url: "/Twitter/AuthorizeCallback?verificationCode=" + code,
            type: "GET",
            success: function (data) {
                $(Hackathon.Dom.loadingLogin).fadeOut(300, function () {
                    if (data === true) {
                        getTweets(currentPage);
                    }
                    else {
                        toggleError(true, "Verification code incorrect!");
                    }
                });
            },
            error: function (error) {
                toggleError(true, "Twitter verification failed!");
                $(Hackathon.Dom.loadingLogin).fadeOut();
            }
        });
    },

    checkAuthentication = function () {
        $.ajax({
            url: "/Twitter/IsAuthorized",
            type: "GET",
            success: function (data) {
                Hackathon.View.toggleTwitter(data);
                if (data) {
                    getTweets(currentPage);
                    setTimeout(Hackathon.View.loading.twitterComplete, 1000);
                }
                else {
                    Hackathon.View.loading.twitterComplete();
                }
            }
        });
    },

    toggleError = function (show, error) {
        var errorContainer = $(Hackathon.Dom.twitterContainer).find(".control-group"),
            errorElement = errorContainer.find(".help-block");
        if (show) {
            errorContainer.addClass("error");
            errorElement.html(error);
        }
        else {
            errorContainer.removeClass("error");
            errorElement.html("");
        }
    },

    getTweets = function (page) {
        $.ajax({
            url: "/Twitter/GetTweets?page=" + page,
            type: "GET",
            success: function (data) {
                $.each(data, function (idx, el) {
                    var tweet = new Hackathon.Models.Tweet(
                        el.User.Name,
                        el.User.ScreenName,
                        "http://twitter.com/" + el.User.ScreenName,
                        el.User.ProfileImageUrl,
                        el.TextAsHtml
                    );
                    Hackathon.View.addTweet(tweet);
                });
                Hackathon.View.toggleTwitter(true);
                $(Hackathon.Dom.loadingMore).fadeOut();
            },
            error: function (error) {
                Hackathon.View.toggleTwitter(false);
                $(Hackathon.Dom.loadingMore).fadeOut();
            }
        });
    };

    return {

        init: function () {
            bind();
            checkAuthentication();
        }
    };
})();


Hackathon.StackOverflow = (function () {

    var bind = function () {
        $(document).on("click", Hackathon.Dom.buttonSearch, search);
    },

    // https://api.stackexchange.com/docs/search#order=desc&sort=activity&tagged=.net&filter=default&site=stackoverflow
    search = function () {
        var searchFilter = $(Hackathon.Dom.inputSearch).val();
        $.ajax({
            url: "/StackOverflow/GetStackQuestions?tag=" + searchFilter,
            type: "GET",
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
            }
        });
    };

    return {

        init: function () {
            SE.init({
                clientId: 234,
                key: ")0Opb*yy5HhC14MJuUKNGg((",
                channelUrl: 'http://hackathon/',
                complete: function (data) {
                }
            });
            bind();
        }
    };
})();

$(function () {
    Hackathon.View.init();
    Hackathon.Server.init();
    Hackathon.Chat.init();
    Hackathon.Twitter.init();
    Hackathon.StackOverflow.init();
    setTimeout(function() { $(Hackathon.Dom.loadingModal).fadeOut(200); }, 1500);
});