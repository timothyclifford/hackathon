﻿using System.Threading.Tasks;
using SignalR;

namespace Hackathon.Signalr
{
    public class ChatConnection : PersistentConnection
    {
        protected override Task OnReceivedAsync(string connectionId, string data)
        {
            return Connection.Broadcast(data);
        }
    }
}
