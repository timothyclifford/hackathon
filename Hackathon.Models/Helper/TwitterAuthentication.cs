﻿using System.Web.Configuration;

namespace Hackathon.Models.Helper
{
    public static class TwitterAuthentication
    {
        public static readonly string Key;
        public static readonly string Secret;

        static TwitterAuthentication()
        {
            Key = WebConfigurationManager.AppSettings["TwitterKey"];
            Secret = WebConfigurationManager.AppSettings["TwitterSecret"];
        }
    }
}
