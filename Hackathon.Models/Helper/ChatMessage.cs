﻿using System.Collections.Generic;

namespace Hackathon.Models.Helper
{
    public class ChatMessage
    {
        public string from { get; set; }
        public List<string> to { get; set; }
        public string message { get; set; }
    }
}
