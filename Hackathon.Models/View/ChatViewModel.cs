﻿using System.Collections.Generic;
using Hackathon.Models.Helper;
using TweetSharp;

namespace Hackathon.Models.View
{
    public class ChatViewModel
    {
        public List<ChatMessage> Messages { get; set; }
        public IEnumerable<TwitterStatus> Tweets { get; set; }
        public bool TwitterAuthorized { get; set; }
        public bool YammerAuthorized { get; set; }
    }
}
